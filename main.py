import subprocess
import argparse
import os


def create_parser():
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('path_to_program')
    args_parser.add_argument('-u', '--url_to_project', nargs='?')
    args_parser.add_argument('-d', '--path_for_download', nargs='?')
    args_parser.add_argument('-p', '--path_to_folder', nargs='?')
    args_parser.add_argument('-o', '--output', nargs='?')
    args_parser.add_argument('-r', '--report', action='store_true', help='Select this flag if you want to get detailed reports')
    return args_parser


def get_file_names(folder_name):
    files = os.listdir(folder_name)
    targey_files = []

    for name in files:
        if name[-3:] == '.py':
            targey_files.append(name)

    return targey_files


def start_target_program(program, files, prefix_path, report):
    number_of_files = files.__len__()
    comparse_matrix = [[0 for i in range(number_of_files)] for j in range(number_of_files)]

    os.chdir(prefix_path)

    for i in range(number_of_files):
        for j in range(number_of_files):
            if i >= j:

                # first_file = str(files[i])
                # second_file = str(files[j])

                result_call = subprocess.run(["python3", program, files[i], files[j]], stdout=subprocess.PIPE, text=True)
                res = result_call.stdout

                if float(res) > comparse_matrix[i][j]:
                    comparse_matrix[i][j] = comparse_matrix[j][i] = res

                if report:
                    if i > j:
                        if float(comparse_matrix[i][j]) > 50.0:

                            report_call = subprocess.run(["python3", program, files[i], files[j], '-r'],
                                                          stdout=subprocess.PIPE,
                                                          text=True)
                            report_text = report_call.stdout
                            report_name = str(files[j]) + '-' + str(files[i]) + '.txt'
                            f = open(report_name, 'w')
                            f.write(report_text)
                            # print(os.getcwd())





    return comparse_matrix


def print_result(matrix):
    print("Comparison result:")
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            print(matrix[i][j], end=' ')
        print()


def write_result(output, matrix):
    f = open(output, 'w')
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            f.write(str(matrix[i][j]) + ' ')
        f.write('\n')


def circumcision_url(url):
    a = url.rfind('/')
    if url[-4:] == '.git':
        scrap = url[a + 1:-4]
    else:
        scrap = url[a + 1:]
    return scrap


if __name__ == '__main__':
    parser = create_parser()
    namespace = parser.parse_args()

    if namespace.url_to_project:
        os.chdir(namespace.path_for_download)
        os.system('git clone {0}'.format(namespace.url_to_project))
        name_of_project = circumcision_url(namespace.url_to_project)
        namespace.path_to_folder = namespace.path_for_download + '/' + name_of_project

    else:
        slash = namespace.path_to_folder.rfind('/')
        name_of_project = namespace.path_to_folder[slash+1:]
        os.chdir(namespace.path_to_folder)
        os.chdir('..')

    list_of_folder = os.listdir(name_of_project)
    os.chdir(name_of_project)

    for folder_name in list_of_folder:
        if folder_name[-4:]!='.git':
            files = get_file_names(folder_name)
            os.chdir('..')
            os.chdir('..')

            matrix = start_target_program(namespace.path_to_program, files, namespace.path_to_folder+'/'+folder_name,
                                          namespace.report)
            print_result(matrix)

            os.chdir(namespace.path_to_folder + '/' + folder_name)
            if namespace.output:
                write_result(namespace.output, matrix)

            os.chdir('..')


    # if namespace.path_to_program:
        # print(namespace.path_to_program)

    # if namespace.path_to_directory:
        # print(namespace.path_to_directory)

    # print(platform.system())
    # print(subprocess.check_output('python check.py 1.py 2.py'))

